module.exports = {
  publicPath: './',//根路径 cli3.0以上使用publicPath替代baseUrl,解决build后找不到静态资源的问题
  //assetsDir:'assets',//静态资源目录(js,css,img,fonts)这些文件都可以写里面
  outputDir: 'dist',//打包的时候生成的一个文件夹名
  lintOnSave: false,//是否开启eslint保存检测 ,它的有效值为 true || false || 'error'
  devServer: {
    proxy: {
      '/': {
        target: 'http://api-toutiao-web.itheima.net',
        ws: true,
        changeOrigin: true
      },
  }
  }
}
