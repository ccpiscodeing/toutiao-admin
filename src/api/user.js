import request from "../utils/request";

// 登录接口
export const login = (data)=>{
  return request({
    method: 'POST',
    url: '/mp/v1_0/authorizations',
    // data 用来设置 POST 请求体
    data
  })
}
// 获取用户信息
export const getUserProfile = ()=>{
  return request({
    method:'GET',
    url:'/mp/v1_0/user/profile',
  })
}
// 获取用户信息
export const updateUserPhoto = (data)=>{
  return request({
    method:'PATCH',
    url:'/mp/v1_0/user/photo',
    data
  })
}
// 修改用户基本信息
export const updateUserProfile = (data)=>{
  return request({
    method:'PATCH',
    url:'/mp/v1_0/user/profile',
    data
  })
}
