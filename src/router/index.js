import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/login/";
import Test from "@/views/Test/";
import Layout from "@/views/layout/";
import Home from "@/views/home/"
import Article from '@/views/article/'
import Publish from '@/views/publish/'
import Image from '@/views/image/'
import Setting from '@/views/setting/'
import Comment from '@/views/comment/'
import Fan from '@/views/fan/'

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/",
    name: "layout",
    component: Layout,
    children: [
      {
        path: "",
        component:Home
      },
      {
        path:'article',
        component: Article
      },
      {
        path: 'publish',
        component: Publish
      },
      {
        path: 'image',
        component: Image
      },
      {
        path:'comment',
        component:Comment
      },
      {
        path:'settings',
        component:Setting
      },
      {
        path:'fan',
        component:Fan
      },
    ]
  },
  {
    path: "/test",
    name: "test",
    component: Test
  }
];
// 不能写在这里，router不然拿不到，要写在router里面
// const user = JSON.parse(window.localStorage.getItem('user'))
const router = new VueRouter({
  routes
});
router.beforeEach((to,from,next)=>{
  const user = JSON.parse(window.localStorage.getItem('user'))
  if(to.path!=='/login'){
   if(user){
     next()
   }else {
     next('/login')
   }
  }else{
    next()
  }
})
export default router;
