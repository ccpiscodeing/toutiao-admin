import axios from "axios";
import JSONbig from "json-bigint";
import router from "../router";
import {Message} from "element-ui";

const request = axios.create({
  baseURL: "", // 最新接口地址
  transformResponse: [function(data) {
    try {
      return JSONbig.parse(data);
    } catch (e) {
      return data;
    }
  }]
});
request.interceptors.request.use(
  config => {
    const user = JSON.parse(window.localStorage.getItem("user"));
    if (user) {
      config.headers.Authorization = `Bearer ${user.token}`;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

request.interceptors.response.use(
  response => {
      return response
  }, error => {
    if(error?.response?.status ===401){
      window.localStorage.removeItem('user')
      router.push('/login')
      Message.error('登录状态无效,请重新登录')
    }
    console.log("状态码异常");
    return Promise.reject(error)
  }
);
// 导出请求方法
export default request;
